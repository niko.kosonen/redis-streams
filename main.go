package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"redis-streams/consumer"
	"redis-streams/producer"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/gorilla/websocket"
)

var NumConsumers = 4

// WSClient sends and receives WS messages
type WSClient struct {
	connection *websocket.Conn
	done       chan bool
}

// WSServer listens and serves WS
type WSServer struct {
	clients   []*WSClient
	broadcast chan []byte
	consumers map[int]*consumer.Consumer
}

func checkOrigin(r *http.Request) bool {
	origin := r.Header.Get("Origin")
	if origin == "http://localhost:8000" || origin == "file://" {
		return true
	}
	return false
}

var upgrader = websocket.Upgrader{
	CheckOrigin:     checkOrigin,
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	Subprotocols:    []string{"binary"},
}

func (ws *WSServer) connect(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Hello")

	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade error:", err)
		return
	}

	log.Println("client connected")
	client := &WSClient{connection: c}
	ws.clients = append(ws.clients, client)
	ws.listen(client)

}

func (ws *WSServer) listen(client *WSClient) {

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	client.done = make(chan bool)

	go func(server *WSServer, client *WSClient) {
		select {
		case <-interrupt:
			log.Println("Got interrupt signal, closing websocket connection")
			ws.close(client)
			select {
			case <-client.done:
				return
			case <-time.After(time.Second):
				return
			}
		case <-client.done:
			return
		}
	}(ws, client)

	go func(server *WSServer, client *WSClient) {
		for {
			select {
			case <-client.done:
				return
			default:
				fmt.Println("client waiting for messages")

				_, message, err := client.connection.ReadMessage()
				if err != nil {
					log.Println("read error:", err)
					server.close(client)
					return
				}
				s := string(message)
				log.Printf("recv: %s", message)

				switch s[:6] {
				case "toggle":
					i, err := strconv.Atoi(s[6:])
					if err != nil {
						fmt.Println("tried to toggle client with command: ", s)
						break
					}
					ws.consumers[i].ToggleStream()
				}
			}

		}
	}(ws, client)

	go func(server *WSServer, client *WSClient) {
		for {
			select {
			case msg := <-server.broadcast:

				err := client.connection.WriteMessage(websocket.TextMessage, msg)
				if err != nil {
					fmt.Println("listener error", err)
					ws.close(client)
					server.broadcast <- msg
					break
				}
			case <-client.done:
				return
			}
		}
	}(ws, client)

}

func (ws *WSServer) close(client *WSClient) {

	defer client.connection.Close()
	defer close(client.done)

	fmt.Println("Closing WS")

	err := client.connection.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		log.Println("WS closing error:", err)
		return
	}
}

func main() {

	redis := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0, // use default DB
	})

	ws := &WSServer{
		broadcast: make(chan []byte, 512),
		consumers: make(map[int]*consumer.Consumer),
	}

	producer := &producer.Producer{Client: redis, Broadcast: ws.broadcast}

	ticker := time.NewTicker(1000 * time.Millisecond)
	go func() {
		for range ticker.C {
			producer.AddItem()
		}
	}()

	for i := 0; i < NumConsumers; i++ {
		c := &consumer.Consumer{ID: i + 1, Client: redis, Broadcast: ws.broadcast}
		ws.consumers[c.ID] = c
	}

	producer.CreateGroup("group-1")
	ws.consumers[1].Group = "group-1"
	ws.consumers[2].Group = "group-1"

	producer.CreateGroup("group-2")
	ws.consumers[3].Group = "group-2"
	ws.consumers[4].Group = "group-2"

	for _, c := range ws.consumers {
		c.Startup()
	}

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		ws.connect(w, r)
	})

	http.ListenAndServe(":3001", nil)

	fmt.Println("READY")

	done := make(chan bool, 1)
	<-done

}
