package producer

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"github.com/go-redis/redis"
)

type JSONData struct {
	Producer ProducerData `json:"producer"`
}

type ProducerData struct {
	Message string `json:"message"`
	//Status  int    `json:"status"`
}

//Producer adds items to stream
type Producer struct {
	Broadcast chan []byte
	Client    *redis.Client
	Ticker    int
}

//AddItem adds an item to stream
func (p *Producer) AddItem() {

	p.Ticker++

	cmd := p.Client.XAdd(&redis.XAddArgs{
		Stream: "ticker",
		Values: map[string]interface{}{
			"data": p.Ticker,
		},
	})

	_, err := cmd.Result()
	if err != nil {
		fmt.Println("add item error:", err)
		return
	}

	bytes, err := json.Marshal(&JSONData{Producer: ProducerData{Message: strconv.Itoa(p.Ticker)}})
	if err != nil {
		log.Fatal(err)
	}

	select {
	case p.Broadcast <- bytes:
	default:
	}

}

func (p *Producer) CreateGroup(group string) {

	// $ == this group should consume only new messages
	cmd := p.Client.XGroupCreate("ticker", group, "$")
	_, err := cmd.Result()
	if err != nil {
		fmt.Println("create group error:", err)
		return
	}
}
