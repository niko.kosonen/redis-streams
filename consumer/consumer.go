package consumer

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"github.com/go-redis/redis"
)

type ConsumerData struct {
	ID      int    `json:"id"`
	Message string `json:"message"`
	Status  int    `json:"status"`
}

type JSONData struct {
	Consumer ConsumerData `json:"consumer"`
}

//Consumer reads items from stream
type Consumer struct {
	ID        int
	Group     string
	Client    *redis.Client
	Broadcast chan []byte
	Done      chan bool
	Status    int
}

func (c *Consumer) Startup() {

	c.Done = make(chan bool, 1)
	c.Status = 1

	bytes, err := json.Marshal(&JSONData{Consumer: ConsumerData{ID: c.ID, Status: c.Status, Message: "Connected"}})
	if err != nil {
		log.Fatal(err)
	}

	c.Broadcast <- bytes

	go c.ConsumeStream()
}

//ConsumeStream starts reading a stream
func (c *Consumer) ConsumeStream() {

	for {
		select {
		case <-c.Done:
			return
		default:
		}

		id := strconv.Itoa(c.ID)

		// > == only get messages not consumed by any consumer in this group
		stream, err := c.Client.XReadGroup(&redis.XReadGroupArgs{
			Group:    c.Group,
			Consumer: "consumer-" + id,
			Streams:  []string{"ticker", ">"},
			Block:    1000}).Result()

		if err != nil {
			fmt.Println("error reading stream: ", err)
		}

		ackIDs := []string{}
		for _, m := range stream[0].Messages {

			data := m.Values["data"]

			bytes, err := json.Marshal(&JSONData{Consumer: ConsumerData{ID: c.ID, Status: c.Status, Message: data.(string)}})
			if err != nil {
				log.Fatal(err)
			}

			c.Broadcast <- bytes
			ackIDs = append(ackIDs, m.ID)
		}

		c.Client.XAck(stream[0].Stream, c.Group, ackIDs...)

	}

}

func (c *Consumer) ResumeStream() {
	c.Done = make(chan bool, 1)
	c.Status = 1
	bytes, err := json.Marshal(&JSONData{Consumer: ConsumerData{ID: c.ID, Status: c.Status, Message: "resuming stream"}})
	if err != nil {
		log.Fatal(err)
	}

	c.Broadcast <- bytes
	go c.ConsumeStream()
}

func (c *Consumer) StopStream() {

	c.Status = 0

	bytes, err := json.Marshal(&JSONData{Consumer: ConsumerData{ID: c.ID, Status: c.Status, Message: "disconnected"}})
	if err != nil {
		log.Fatal(err)
	}

	c.Broadcast <- bytes

	c.Done <- true
	close(c.Done)
}

func (c *Consumer) ToggleStream() {
	if c.Status == 0 {
		fmt.Println("RESUMING STREAM")
		c.ResumeStream()
	} else {
		fmt.Println("STOPPING STREAM")

		c.StopStream()
	}
}
